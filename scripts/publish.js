const mdToPdf = require('md-to-pdf');
const fs = require('fs');

const DIST_FOLDER = `${__dirname}/../dist`;

if (!fs.existsSync(DIST_FOLDER)){
  fs.mkdirSync(DIST_FOLDER);
}

const INPUT_MARKDOWN_FILE = `${__dirname}/../src/RESUME.md`;

const RESUME_FILENAME = `Dak_Washbrook_Resume.pdf`;
const DESTINATION = `${DIST_FOLDER}/${RESUME_FILENAME}`;

mdToPdf(INPUT_MARKDOWN_FILE, {
  dest: DESTINATION,
  stylesheet: [
    'https://fonts.googleapis.com/css?family=Open+Sans',
    'node_modules/github-markdown-css/github-markdown.css',
    'https://pro.fontawesome.com/releases/v5.8.1/css/all.css',
    'src/style.css'
  ],
  body_class: "markdown-body",
  stylesheet_encoding: "utf-8",
  pdf_options: {
    format: "A4",
    margin: "5mm 8mm"
  },
})
  .then(pdf => console.log('Created:', pdf.filename))
  .catch(console.error);