# Dak Washbrook
<left>
<i class="fas fa-home"></i> &nbsp;&nbsp;<a href="https://goo.gl/maps/CPYh2YCW5CM37s6b7" target="_blank">Aliso Viejo, CA</a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <i class="fas fa-phone fa-flip-horizontal"></i> &nbsp;&nbsp;<a href="tel:+18185176315">818-517-6315</a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <i class="fas fa-envelope"></i> &nbsp;&nbsp;<a href="mailto:me@dak.dev">me@dak.dev</a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <i class="fab fa-bitbucket"></i> &nbsp;&nbsp;<a href="https://bitbucket.org/vinlockz/" target="_blank">bitbucket.org/vinlockz</a> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <span class="oses"><i class="fab fa-apple"></i> &nbsp;&nbsp;<i class="fab fa-linux"></i> &nbsp;&nbsp;<i class="fab fa-windows"></i></span>
</left>  

## Work Experience

<h3><i class="fas fa-building"></i> <a href="https://us.ncsoft.com" target="_blank">NCSOFT Interactive</a></h3>

<i class="fas fa-briefcase"></i> **Senior Web Engineer** / `January 2019 - Present`  
- Took ownership of, engineered, and maintained containerized and scalable Node.js applications that were utilized by client-side engineers to service customer-facing web and in-game interface applications. Applications included REST and GraphQL API interfaces.  
- Collaborated with QA teams and stakeholders to uphold project requirements while producing results of the highest possible quality.
- Responsible for growing and maintaining the core relationship between our team and the Platform team.
- Built, maintained, and scaled out cloud infrastructure on AWS for front-end and back-end/server-side applications and services.
- Designed and developed a Platform SDK for JavaScript in NodeJS as a wrapper around the platform services provided by on and off-site platform teams and utilized by our team. The SDK exposed a set of JavaScript APIs, as well as abstracted session ID encryption algorithms ported from Java, to easily interface with all necessary platform functionality. 
- Project Lead for integrating CI/CD into development and publishing workflows and practices. Designed and created Jenkins CI/CD workflows, scripts, and shared libraries in Groovy.  
- Worked jointly with other teams and stakeholders on projects to maintain an understanding of the possibilities and timelines of their requests. Contributed technical input on projects based on experience and market research. Set new company standards of documentation to help connect technical and non-technical teams across the organization.  

<i class="fas fa-briefcase"></i> **Web Engineer** / `November 2016 - January 2019`  
- Refactored the RESTful API built on the Phalcon PHP Framework to better support scalability and cash purchase integration with the Platform and front-end clients.
- Developed, deployed, and maintained custom SSO Auth integration and multiple functional features on top of Invision Power Board forum software on a per product basis for customers to have a more personal and game-integrated experience.  
- Pioneered the use of GraphQL at the company and utilized it where it made sense. Allowed the front-end developers to optimize their API calls to increase their application's performance and user experience.

<h3><i class="fas fa-building"></i> <a href="https://www.dellamoda.com">Dellamoda, Inc.</a></h3>

<i class="fas fa-briefcase"></i> **Lead Web Developer** / `October 2010 - November 2016`  
- Developed and maintained tools to automate and exponentially increase efficiency across multiple internal workflows. Developed a scalable application and RESTful API on the Laravel PHP Framework to defeat the limitations of legacy software employed by the company. The API housed multiple customer-facing user interfaces and dealt with automating processes across the company.

## Projects

<table class="mktbl top-table" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td>
        <h3><i class="fas fa-project-diagram"></i> Website (In Progress) - <i>I Want Lasers</i></h3>
        <b>Web Developer</b> / <code>March 2019 - Present</code>
        <ul>
            <li>Designed and Developed a SEO conscious marketing website using React JS, Gatsby JS, Express JS, and GraphQL.</li>
            <li>Assisted with guiding the brand direction for the website design and user experience.</li>
        </ul>
        <h3><i class="fas fa-project-diagram"></i> Discord Bot - <i>Imperial Gaming Network</i></h3>
        <b>Creator / Developer</b> / <code>February 2016</code>
        <ul>
            <li>Utilized for chat moderation by server admins and moderators.</li>
            <li>The bot managed a full match/betting system that maintained point totals for all Discord server members to assist with user interaction during game tournaments hosted by Imperial.</li>
            <li>Housed many other mini-games that over 500 unique users played with and versus each other daily in the chat using the previously mentioned points system.</li>
            <li><b>Tech:</b> Python and MySQL Database</li>
        </ul>
    </td>
    <td>
<h3><i class="fas fa-project-diagram"></i> Website - <i><a href="https://zephyrgaming.gg/" target="_blank">Zephyr Gaming Community</a></i></h3>
<b>Founder / Developer</b> / <code>October 2018 - Present</code>
<ul>
<li>Developed the core website for the Zephyr Gaming Community in React JS with a NodeJS Express JS backend REST API.</li>
<li>Created a translator and real-time schedule to serve the purpose of assisting gamers with the game Aion Online.</li>
</ul>
<h3><i class="fas fa-project-diagram"></i> Website - <i>PvP All Day</i></h3>
<b>Founder / Developer</b> / <code>September 2010 - November 2014</code>
<ul>
<li>Developed a video showcase website delivering accurately categorized gaming video content to users.</li>
<li>The website reached 20,000 unique visitors per month based on Google Analytics. Users visited from locations across the world.</li>
<li><b>Tech:</b> First developed in raw PHP, later re-developed under Laravel. Serviced by a MySQL database.</li>
</ul>  
</td>
</tr>
</table>

<table class="mktbl bottom-table" border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
<h2>Technical Skills</h2>
<b><i class="fas fa-code"></i> Languages:</b> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">JavaScript</a>, <a href="https://php.net/" target="_blank">PHP</a>, <a href="https://www.python.org/" target="_blank">Python</a>, <a href="https://en.wikipedia.org/wiki/SQL" target="_blank">SQL</a>, <a href="https://www.gnu.org/software/bash/" target="_blank">Bash</a>, <a href="http://groovy-lang.org/" target="_blank">Groovy</a>, <a href="https://en.wikipedia.org/wiki/HTML" target="_blank">HTML</a>, <a href="https://developer.mozilla.org/en-US/docs/Web/CSS" target="_blank">CSS</a> & <a href="https://sass-lang.com/" target="_blank">SASS</a><br />  
<b><i class="fas fa-puzzle-piece"></i> Frameworks / Libraries:</b> <a href="https://expressjs.com/" target="_blank">ExpressJS</a>, <a href="https://github.com/graphql/graphql-js" target="_blank">GraphQL JS</a> & <a href="https://www.apollographql.com/" target="_blank">Apollo</a>, <a href="https://jestjs.io/" target="_blank">Jest</a>, <a href="https://mochajs.org/" target="_blank">Mocha</a> & <a href="https://www.chaijs.com/" target="_blank">Chai</a>, <a href="https://reactjs.org/" target="_blank">React JS</a>, <a href="https://www.gatsbyjs.org/" target="_blank">GatsbyJS</a>, <a href="https://laravel.com/" target="_blank">Laravel</a> & <a href="https://lumen.laravel.com/" target="_blank">Lumen</a>, <a href="https://phalconphp.com/en/" target="_blank">Phalcon</a><br />
<b><i class="fas fa-cogs"></i> Technologies:</b> <a href="https://nodejs.org/en/" target="_blank">NodeJS</a>, <a href="https://www.mysql.com/" target="_blank">MySQL</a>, <a href="https://www.mongodb.com/" target="_blank">MongoDB</a>, <a href="https://graphql.org/" target="_blank">GraphQL</a>, <a href="https://www.nginx.com/" target="_blank">NGINX</a>, <a href="https://kubernetes.io/" target="_blank">Kubernetes</a>, <a href="https://www.docker.com/" target="_blank">Docker</a><br />  
<b><i class="fas fa-tools"></i> Tools:</b> <a href="https://en.wikipedia.org/wiki/Command-line_interface" target="_blank">Linux CLI</a>, <a href="https://git-scm.com/" target="_blank">Git</a> (<a href="https://github.com" target="_blank">Github</a>, <a href="https://www.perforce.com/video-tutorials/gitswarm-overview" target="_blank">Gitswarm</a>, <a href="https://bitbucket.org" target="_blank">Bitbucket</a>), <a href="https://jenkins.io/" target="_blank">Jenkins</a>, <a href="https://www.atlassian.com/software/jira" target="_blank">JIRA</a>, <a href="https://www.atlassian.com/software/confluence" target="_blank">Confluence</a><br />  
<b><i class="fas fa-cloud"></i> Cloud Services:</b> <a href="https://aws.amazon.com/" target="_blank">AWS</a>, <a href="https://www.digitalocean.com/" target="_blank">DigitalOcean</a>, <a href="https://www.netlify.com/" target="_blank">Netlify</a><br />
</td>
<td>
<h2>Personal Traits</h2>
<b><i class="far fa-smile-beam"></i></b> Highly self-motivated, passionate, and consistently excited to continously learn new things.<br />
<b><i class="fas fa-tachometer-alt-fast"></i></b> Fast learner, always tinkering, and very analytical, logical, and computational thinker.<br />
<b><i class="fas fa-football-helmet"></i></b> At the ready to tackle any problems head-on while inventing and implementing efficient and effective solutions.<br />
<b><i class="fas fa-tv-retro"></i></b> Avid watcher of sci-fi, fantasy, law, and police shows/movies.<br />
<b><i class="fas fa-laptop"></i></b> Tech Enthusiast.<br />
<b><i class="fas fa-gamepad"></i></b> Frequent gamer.
</td>
</tr>
</table>  